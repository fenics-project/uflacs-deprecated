uflacs.backends.ffc package
===========================

Submodules
----------

uflacs.backends.ffc.access module
---------------------------------

.. automodule:: uflacs.backends.ffc.access
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ffc.common module
---------------------------------

.. automodule:: uflacs.backends.ffc.common
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ffc.definitions module
--------------------------------------

.. automodule:: uflacs.backends.ffc.definitions
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ffc.ffc_compiler module
---------------------------------------

.. automodule:: uflacs.backends.ffc.ffc_compiler
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ffc.generation module
-------------------------------------

.. automodule:: uflacs.backends.ffc.generation
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ffc.representation module
-----------------------------------------

.. automodule:: uflacs.backends.ffc.representation
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: uflacs.backends.ffc
    :members:
    :undoc-members:
    :show-inheritance:
