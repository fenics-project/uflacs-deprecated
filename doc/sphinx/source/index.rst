.. title:: UFL Analyser and Compiler System

UFL Analyser and Compiler System (UFLACS)
=========================================


Uflacs, the UFL Analyser and Compiler System, is a collection of
algorithms for processing symbolic UFL forms and expressions. The main
feature is efficient translation of tensor intensive symbolic
expressions into a low level expression representation and C++ code.
It is part of the FEniCS Project (http://fenicsproject.org).


Installing
----------

Either install to default Python location as root::

  sudo python setup.py install

Or install to your own Python path directory::

  python setup.py install --prefix=/path/to/my/own/site-packages


Help and support
----------------

Send help requests and questions to fenics-support@googlegroups.com,
and send feature requests and questions to
fenics-dev@googlegroups.com.


Development and reporting bugs
------------------------------

The git source repository for UFLACS is located at
https://bitbucket.org/fenics-project/uflacsa.  Bugs can be registered
at https://bitbucket.org/fenics-project/uflacs/issues.

For general UFLACS development questions and to make feature requests,
use fenics@fenicsproject.org.


Documentation
-------------

.. toctree::
   :titlesonly:

   api-doc/uflacs
   releases

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
